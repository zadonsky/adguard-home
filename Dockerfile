FROM alpine
MAINTAINER <d.zadonskii>

ENV ADGUARD_VERSION=v0.92-hotfix2

RUN adduser -D adguard
EXPOSE 10053 3000
WORKDIR /tmp
RUN apk --no-cache add ca-certificates && \
    wget https://github.com/AdguardTeam/AdGuardHome/releases/download/${ADGUARD_VERSION}/AdGuardHome_${ADGUARD_VERSION}_linux_amd64.tar.gz && \
    tar xvf AdGuardHome_${ADGUARD_VERSION}_linux_amd64.tar.gz && \
    mv AdGuardHome/AdGuardHome /home/adguard/ && \
    rm -fr AdGuardHome_${ADGUARD_VERSION}_linux_amd64.tar.gz AdGuardHome

WORKDIR /home/adguard/
USER adguard

CMD ["/home/adguard/AdGuardHome", "-c", "config/AdGuardHome.yaml"]

